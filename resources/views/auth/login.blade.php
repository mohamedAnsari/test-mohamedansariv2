

@extends('includes.master')
@section('content')
<div class="jumbotron">
      <div class="container">
<form class="form-horizontal"  method="POST" action="/auth/login">
   {!! csrf_field() !!}
<fieldset>


<legend>Login</legend>

<div class="form-group">
  <label class="col-md-4 control-label" for="">Username</label>
  <div class="col-md-4">
  <input type="email" name="email" value="{{ old('email') }}" class="form-control input-md">

  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="">Password</label>
  <div class="col-md-4">
    <input type="password" name="password" id="password" class="form-control input-md">

  </div>
</div>




<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-8">
    <input type="checkbox" name="remember"> Remember Me
    <button class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-warning">Login</button>
  </div>
</div>

</fieldset>
</form>
@endsection
