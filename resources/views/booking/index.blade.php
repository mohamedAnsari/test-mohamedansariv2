@extends('includes.master')
@section('css')

@endsection
@section('content')
@if(!empty($bookings))
<form method="get" action="/bookings">

  <input type="text" placeholder="search name, phone" name="search">
  <button type="submit" > search </button>
</from>
<div class="jumbotron">
      <div class="container">


        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Pitch Id</th>
                <th>Price</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Pitch Id</th>
              <th>Price</th>
            </tr>
        </tfoot>
        <tbody>
          @foreach($bookings as $booking)
            <tr>
                <td>{{$booking->id}}</td>
                <td>{{$booking->user->first_name}} {{$booking->user->last_name}}</td>
                <td>{{$booking->pitch_id}} </td>
                <td>{{$booking->price}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
{!! $bookings->render() !!}

@else
<p> no data <p>
  @endif
@endsection
@section('js')

@endsection
