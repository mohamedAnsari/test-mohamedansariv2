@extends('includes.master')
@section('css')


@endsection

@section('content')
@if(!empty($users))
<form method="get" action="/users">
  
  <input type="text" placeholder="search name, phone" name="search">
  <button type="submit" > search </button>
</from>
<div class="jumbotron">

      <div class="container">

        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone Number</th>
                <th>Country</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Pitch Id</th>
              <th>Price</th>
            </tr>
        </tfoot>
        <tbody>
          @foreach($users as $user)
            <tr>
                <td>{{$user->first_name}}</td>
                <td> {{$user->last_name}}</td>
                <td>{{$user->phone}} </td>
                <td>{{$user->country->name}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
{!! $users->render() !!}

@else
<p> no data <p>
  @endif
@endsection
@section('js')

@endsection
