@if ($errors->any())
  <ul class="alert alert-danger margin-bottom-30">
    <strong>Missing Data!</strong>
    <div class="padding-20">
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  </ul>
@endif
