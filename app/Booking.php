<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  protected $fillable = ['id', 'user_id','price','pitch_id'];
  public function user(){
    return $this->belongsTo('App\User');
  }

}
