<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      \App\Booking::creating(function ($booking) {

          \Log::info(' [create] '."{".$booking->id."} ". date("Y-m-d H:i:s"));
      });
      \App\Booking::updating(function ($booking) {

          \Log::info(' [update] '."{".$booking->id."} ". date("Y-m-d H:i:s"));
      });
      \App\Booking::deleting(function ($booking) {

          \Log::info(' [delete] '."{".$booking->id."} ". date("Y-m-d H:i:s"));
      });
      \Log::useFiles(storage_path('logs/bookings.log'));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
