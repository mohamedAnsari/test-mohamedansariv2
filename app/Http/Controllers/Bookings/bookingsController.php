<?php

namespace App\Http\Controllers\Bookings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Booking;
use App\User;
use App\Country;
use View;
class bookingsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index()
  {
    if(request()->has('search')){


          $bookings = Booking::where('pitch_id','like','%'.request()->search.'%')
          ->Where('price','like','%'.request()->search.'%')->orWhereHas('user',function($item){

            $item->where('first_name','like','%'.request()->search.'%');
          })->orWhereHas('user',function($item){

            $item->where('last_name','like','%'.request()->search.'%');
          })
          ->paginate(15);


    }
    else {
      //retrieve all bookings
      $bookings = Booking::paginate(15);
    }

    return View::make('booking.index', array(
        'bookings' => $bookings,
    ));
  }

  public function raw()
  {
    //get and json encode the database records (first_name & last name as name from user tables  , phone from user tables, price from booking table)
    return response()->json(\DB::select('select bookings.id,  CONCAT(users.first_name , " ", users.last_name) as name, users.phone, bookings.price as price  from users, bookings where users.id = bookings.user_id'));

  }
  /**
   * check for new changes in the remote json
   */
  public function checkUpdates()
  {
    //get last booking build hash

    //get remote booking.json content
    //open curl

    $curl = curl_init();
    $result =  file_get_contents('http://beta-tryout.malaebapp.com/bookings.json');
    $result = json_decode($result);

      if($this->checkBuildHash($result->build_hash)){


          $resultCollect = collect($result->data);
          $bookingsCollect = Booking::get()->lists('id')->all();
          //spot the deleted items
          $diff = array_diff($bookingsCollect, $resultCollect->pluck('id')->toArray() );
          $dd = Booking::destroy($diff);

          //check for new and updated records
          foreach($resultCollect as $data){
            //create or update user
            $user = User::updateOrCreate([
              'first_name' => explode(" " , $data->user)[0],
              'last_name' => explode(" " , $data->user)[1],
            ],
            [
              'phone' => $data->phone,
              'country_id'=> (Country::where('currency_code', $data->currency)->first()) ? Country::where('currency_code', $data->currency)->first()->id : 4,
           ]
          );
          //create or update bookings
            $bookings = Booking::updateOrCreate([
              'id'=> $data->id,
            ],[
              'user_id' => $user->id,
              'pitch_id'=>$data->pitch_id,
              'price' => $data->price,

            ]);
          }

    }
    curl_close($curl);
  }

  public function checkBuildHash($new){
    if($this->storeBuild($new, 'booking.txt')){
    $old = \Storage::disk('local')->get('booking.txt');

    if($old !== $new){
      return true;
    }

  }
  else {

    return true;
  }
  return false;
}

  public function storeBuild($build,$fileName){
    //check if the file already exists
    if(!\Storage::disk('local')->has($fileName)){
      \Storage::disk('local')->put($fileName, $build);
      return false;
    }
    return true;
  }
}
