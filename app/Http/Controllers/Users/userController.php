<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use View;
class userController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index()
  {
    //retrieve all bookings

    if(request()->has('search')){


          $users = User::where('first_name','like','%'.request()->search.'%')->orWhere('last_name','like','%'.request()->search.'%')->orWhere('phone','like','%'.request()->search.'%')->paginate(15);


    }
    else {
      $users = User::paginate(15);
    }

    return View::make('user.index', array(
        'users' => $users,
    ));
  }
  public function raw()
  {
    //get and json encode the database records (first_name & last name as name from user tables, phone from user tables, country from countries tables)
    return response()->json(\DB::select('select CONCAT(users.first_name , " ", users.last_name) as name, phone, countries.name as country  from users, countries where users.country_id = countries.id'));

  }
}
